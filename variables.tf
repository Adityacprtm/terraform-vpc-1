variable "region" {
  type        = string
  description = "AWS region"
  default     = "ap-southeast-1"
}

variable "name" {
  type    = string
  default = "example"
}

variable "env" {
  type    = string
  default = "dev"
}

variable "vpc_cidr" {
  type    = string
  default = "10.10.0.0/16"
}

variable "ec2_instance_type" {
  type    = string
  default = "t2.medium"
}

variable "asg_min_size" {
  type    = number
  default = 2
}

variable "asg_max_size" {
  type    = number
  default = 5
}

variable "threshold_cloudwatch_metric_up" {
  type    = string
  default = "45"
}

variable "threshold_cloudwatch_metric_down" {
  type    = string
  default = "10"
}

# variable "public_subnets_cidr" {
#   type    = list(string)
#   default = ["10.10.0.0/24", "10.10.1.0/24", "10.10.2.0/24"]
# }

# variable "private_subnets_cidr" {
#   type    = list(string)
#   default = ["10.10.10.0/24", "10.10.11.0/24", "10.10.12.0/24"]
# }

# variable "availability_zones" {
#   type = list(string)
# }

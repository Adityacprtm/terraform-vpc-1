# Terraform VPC

- Hanya untuk test
- Gitlab CICD running hanya ketika **manual** Run Pipeline, untuk meminimalisir auto deploy
- Terraform state backend in S3

![terraform-vpc](terraform-vpc.png "terraform-vpc")

## Case

- 1 VPC
- 1 subnet public
- 1 subnet private yg terhubung dengan 1 NAT Gateway
- 1 autoscaling group dengan config :
  - EC2 T2.medium
  - minimum 2 instance
  - max 5 instance
  - scaling policy CPU >= 45%
  - in subnet Private

## How to

- Clone untuk provisioning dari local / Fork untuk menggunakan Gitlab CICD

- Local

  - Setup Env Var

    ```sh
    AWS_ACCESS_KEY_ID="xxx"
    AWS_SECRET_ACCESS_KEY="xxx"
    AWS_DEFAULT_REGION="ap-southeast-1"
    ```

  - Terraform command things

    ```sh
    cd terraform-vpc
    terraform init -backend-config="bucket=mybucket"
    terraform plan
    terraform apply
    ```

- Gitlab CICD

  - Setup Env Var in `Gitlab repo - settings - CICD`

    ```sh
    AWS_ACCESS_KEY_ID="xxx"
    AWS_SECRET_ACCESS_KEY="xxx"
    AWS_DEFAULT_REGION="ap-southeast-1"
    ```

  - Change bucket name

  - Run Pipeline manually

## To Do

- [ ] Convert to module based
